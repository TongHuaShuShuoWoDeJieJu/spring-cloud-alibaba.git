package com.ghy.springcloudgoodsservice.goods.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ghy.springcloudgoodsapi.goods.entity.Brand;

public interface BrandMapper extends BaseMapper<Brand> {
}
