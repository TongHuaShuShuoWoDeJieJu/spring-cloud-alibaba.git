package com.ghy.springcloudgoodsservice;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@MapperScan(basePackages = {"com.ghy.springcloudgoodsservice.goods.mapper"})
public class SpringCloudGoodsServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudGoodsServiceApplication.class, args);
    }



}
